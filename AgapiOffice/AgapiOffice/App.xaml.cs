﻿using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using AgapiOffice.ViewModels;
using AgapiOffice.Views;
using Avalonia.Logging.Serilog;
using Serilog;
using AgapiOffice.Misc;

namespace AgapiOffice
{
	public class App : Application
	{
		public override void Initialize()
		{
#if DEBUG
			SerilogLogger.Initialize(new LoggerConfiguration()
				.MinimumLevel.Warning()
				.WriteTo.Trace(outputTemplate: "{Area}: {Message}")
				.CreateLogger());
#endif
			AvaloniaXamlLoader.Load(this);
		}

		public override void OnFrameworkInitializationCompleted()
		{
			if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
			{
				desktop.MainWindow = new MainWindow
				{
					DataContext = new MainWindowViewModel(),
				};
			}

			base.OnFrameworkInitializationCompleted();
		}
	}
}
