﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgapiOffice.Misc
{
	class HomeBoolManager
	{
		private readonly Dictionary<string, bool> homeBools = new Dictionary<string, bool>();

		public void SetBool(string name, bool value)
		{
			if (!homeBools.ContainsKey(name))
				homeBools.Add(name, value);
			if (value)
			{
				foreach (string key in homeBools.Keys.ToArray())
				{
					if (key != name)
						homeBools[key] = false;
					else
						homeBools[key] = true;
				}
			}
			else
				homeBools[name] = value;
		}

		public bool GetValue(string name)
		{
			if(!homeBools.ContainsKey(name))
			{
				homeBools.Add(name, false);
				return false;
			}
			return homeBools[name];
		}

		public void RemoveBool(string name)
		{
			homeBools.Remove(name);
		}

	}
}
