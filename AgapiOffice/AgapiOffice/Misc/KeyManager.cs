﻿using Avalonia.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgapiOffice.Misc
{
	class KeyManager
	{
		private readonly List<Key> keys = new List<Key>();
		private readonly Dictionary<Key[], Func<int>> hotkeys = new Dictionary<Key[], Func<int>>();

		// Adds Hot Keys to dictionary
		public void AddToHotKeys(Key[] keys, Func<int> func)
		{
			hotkeys.Add(keys, func);
		}

		// Remove Hot Keys from dictionary
		public void RemoveHotKeys(Key[] keys)
		{
			hotkeys.Remove(keys);
		}

		// Wrapper for Avalonia Key Event compatiblity
		public void EventAddKey(object sender, KeyEventArgs args)
		{
			AddKey(args.Key);
		}

		// Wrapper for Avalonia Key Event compatiblity
		public void EventRemoveKey(object sender, KeyEventArgs args)
		{
			RemoveKey(args.Key);
		}

		// Add a Key to the Keys List
		public void AddKey(Key key)
		{
			keys.Add(key);
			CheckKeysDown();
		}

		// Remove a Key from the Keys List
		public void RemoveKey(Key key)
		{
			keys.Remove(key);
			CheckKeysDown();
		}

		// Checks the current KeysDown to the HotKeys funtions
		private void CheckKeysDown()
		{
			foreach (Key[] hot in hotkeys.Keys.ToArray())
			{
				if (hot.Length != keys.Count)
					continue;

				bool passed = true;

				foreach (Key key in hot)
				{
					if (!keys.Contains(key))
					{
						passed = false;
						break;
					}
				}
				if (passed)
					hotkeys[hot].Invoke();
			}
		}

	}
}
