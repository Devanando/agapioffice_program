using System.Collections.Generic;
using System;
using System.Text;
using System.Data.SqlTypes;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;
using MySqlConnector;
using AgapiOffice.Misc;

namespace AgapiOffice.Services
{
    class AgapiConnect
    {
        private static MySqlConnection Connect { get; set; }
        public static string Userid { get; set; }
        public static string Username { get; set; }

        public static void CloseConnection()
        {
            Connect.Close();
        }

        public static void OpenConnectionUsers()
        {
            try
            { // host and port number are removed for privacy and security reasons.
                Connect = new MySqlConnection("host=0.0.0.0;port=0;User ID=DEV;Password=test;Database=AgapiUsers;SslMode=None;protocol=socket");
                Connect.Open();
            }
            catch (MySqlException e)
            {
                Out.Debug(e.ToString());
            }
        }

        public static void OpenConnectionDb()
        {
            try
            {

                Connect = new MySqlConnection(""host=0.0.0.0;port=0;;User ID=DEV;Password=test;Database=AgapiOffice;SslMode=None;protocol=socket");
                Connect.Open();
            }
            catch (MySqlException e)
            {
                Out.Debug(e.ToString());
            }
        }

        internal static void Test()
        {
            //throw new NotImplementedException();
        }

        public static int LoginQuery(String login_name, String login_password)
        {
            var cmd = new MySqlCommand();
            String querry = $"SELECT ID FROM users WHERE password='{login_password}' and user='{login_name}'";
            cmd.Connection = Connect;
            cmd.CommandText = querry;
            MySqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                { Userid = reader["ID"].ToString(); }
                reader.Close();
                Connect.Close();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public static string UsernameQuerry()
        {
            try
            {
                var cmd = new MySqlCommand();
                String querry = $"SELECT user FROM acces WHERE userid='{AgapiConnect.Userid}'";
                OpenConnectionDb();
                cmd.Connection = Connect;
                cmd.CommandText = querry;
                MySqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    { AgapiConnect.Username = reader["user"].ToString(); }
                    reader.Close();
                    return AgapiConnect.Username;
                }
                else
                {
                    return "";
                }
            }
            catch (MySqlException ex)
            {
                Out.Debug(ex.ToString());
                return "";
            }
        }

        public static List<string> PopulateDropdown(string name, string table)
        {
            try
            {
                var cmd = new MySqlCommand();
                String querry = $"SELECT {name} FROM {table}";
                OpenConnectionDb();
                cmd.Connection = Connect;
                cmd.CommandText = querry;
                MySqlDataReader reader = cmd.ExecuteReader();
                List<string> dropdown = new List<string>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                        dropdown.Add(reader[name].ToString());
                    reader.Close();
                    return dropdown;
                }
                else
                {
                    return null;
                }
            }
            catch (MySqlException ex)
            {
                Out.Debug(ex.ToString());
                return null;
            }
        }

        public static string SelectXFromXWhereX(string select, string table, string where, string whereis)
        {
            try
            {
                var cmd = new MySqlCommand();
                // example SELECT sellprice FROM shopitems WHERE name='fanta'
                // example SELECT name FROM client WHERE lastname='achternaam'
                // example SELECT stock FROM bringitems WHERE name='handpaper'
                String querry = $"SELECT {select} FROM {table} WHERE {where}='{whereis}'";
                OpenConnectionDb();
                cmd.Connection = Connect;
                cmd.CommandText = querry;
                MySqlDataReader reader = cmd.ExecuteReader();
                string dropdown = "";
                if (reader.HasRows)
                {
                    while (reader.Read())
                        dropdown = reader[select].ToString();
                    reader.Close();
                    return dropdown;
                }
                else
                {
                    return null;
                }
            }
            catch (MySqlException ex)
            {
                Out.Debug(ex.ToString());
                return null;
            }
        }
    }
}
