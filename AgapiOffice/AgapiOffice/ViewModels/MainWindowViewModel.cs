﻿using AgapiOffice.Services;
using AgapiOffice.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace AgapiOffice.ViewModels
{
	public class MainWindowViewModel : ViewModelBase
	{
		public static MainWindow mainWindow;

		private bool _loggedin = false;

		private bool _showhome = false;


		// For whether or not the user has logged in or not
		public bool LoggedIn { get { return _loggedin; } set { this._loggedin = value; OnPropertyChanged(); } }


		public bool ShowHome { get { return _showhome; } set { this._showhome = value; OnPropertyChanged(); } }
	}
}
