﻿using AgapiOffice.Views.Bring;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgapiOffice.ViewModels.Bring
{
	class BringItemViewModel : ViewModelBase
	{
		private string _item = "";
		private bool _checked = false;

		public BringItem BringItem;

		public string Item { get => _item; set { _item = value; OnPropertyChanged(); } }

		public bool Checked { get => _checked; set { _checked = value; OnChecked(value); OnPropertyChanged(); } }

		public void OnChecked(bool value)
		{
			if (value)
				BringItem.DeleteSelf();
		}

	}
}
