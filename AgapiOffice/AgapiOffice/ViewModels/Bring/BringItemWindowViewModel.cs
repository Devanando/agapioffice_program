﻿using AgapiOffice.Views.Bring;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace AgapiOffice.ViewModels.Bring
{
	class BringItemWindowViewModel : ViewModelBase
	{

		private readonly List<BringItem> _visualitems = new List<BringItem>();
		public BringItemWindow window;

		public string[] Rooms => GetRooms();

		public string[] Items => GetItems();

		public List<BringItem> VisualItems => _visualitems;

		private string[] GetItems()
		{
			string[] items = { "Choose an Item", "Soap", "Hand Towels", "Toilet Paper", "Sanitizer" };
			return items;
		}

		public string[] GetRooms()
		{
			string[] rooms = { "Choose a Room", "0", "1", "2", "3" };
			return rooms;
		}

		public void BringItem()
		{
			if (window == null)
				return;
			window.ExitWindow();
		}

	}
}
