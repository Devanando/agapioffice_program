﻿using AgapiOffice.Views;
using AgapiOffice.ViewModels;
using AgapiOffice.Misc;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace AgapiOffice.ViewModels
{
	class MainViewModel : ViewModelBase
	{

		public static MainView mainView;

		private bool _daytoggle = false;
		private bool _nighttoggle = true;

		private readonly HomeBoolManager boolManager = new HomeBoolManager();

		public bool Daytoggle { get => _daytoggle; set { _daytoggle = value; OnPropertyChanged(); } }
		public bool Nighttoggle { get => _nighttoggle; set { _nighttoggle = value; OnPropertyChanged(); } }

		public void DayToggle()
		{
			mainView.DayToggle();
		}

		public void NightToggle()
		{
			mainView.NightToggle();
		}
	}
}
