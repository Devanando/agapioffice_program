﻿using AgapiOffice.Views;
using AgapiOffice.ViewModels;
using AgapiOffice.Misc;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace AgapiOffice.ViewModels
{
	class ClientInfoViewModel : ViewModelBase
	{

		public static ClientInfo clientInfo;

		public string Logo => Directory.GetCurrentDirectory() + "/Assets/avalonia-logo.ico";

		public void GoHome()
		{
			clientInfo.GoHome();
		}

	}
}
