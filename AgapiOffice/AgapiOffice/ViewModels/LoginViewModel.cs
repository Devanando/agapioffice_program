﻿using AgapiOffice.Misc;
using AgapiOffice.Services;
using AgapiOffice.Views;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace AgapiOffice.ViewModels
{
	class LoginViewModel : ViewModelBase
	{
		public static LoginView loginView;
		private string loginName = "";
		private string loginPassword = "";
		private bool wrongPass = false;
		private string output = "";

		public string LoginName { get => loginName; set { loginName = value; OnPropertyChanged(); } }
		public string LoginPassword { get => loginPassword; set { loginPassword = value; OnPropertyChanged(); } }

		public string LoginError => "Error Logging in, please try again";

		// Bool on whether or not to show LogingError
		public bool WrongPass { get => wrongPass; set { wrongPass = value; OnPropertyChanged(); } }

		// Checks whether or not the name and password are correct.
		public void LoginCheck()
		{
			this.output = $"Name = {this.LoginName}, Pass = {this.LoginPassword}";
			AgapiConnect.OpenConnectionUsers();
			if (AgapiConnect.LoginQuery(this.loginName, this.loginPassword) == 1)
				loginView.SetLogged();
			else
				WrongPass = true;
		}
	}
}
