﻿using AgapiOffice.Views.Shop;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgapiOffice.ViewModels.Shop
{
	class ShopItemViewModel : ViewModelBase
	{
		private string _item = "";
		private bool _checked = false;

		public ShopItem ShopItem;

		public string Item { get => _item; set { _item = value; OnPropertyChanged(); } }

		public bool Checked { get => _checked; set { _checked = value; OnChecked(value); OnPropertyChanged(); } }

		public void OnChecked(bool value)
		{
			if (value)
				ShopItem.DeleteSelf();
		}
	}
}
