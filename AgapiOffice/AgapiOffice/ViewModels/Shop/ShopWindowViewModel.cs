﻿using AgapiOffice.Views.Shop;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgapiOffice.ViewModels.Shop
{
	class ShopWindowViewModel : ViewModelBase
	{
		private readonly List<ShopItem> _visualitems = new List<ShopItem>();
		private string _tiptext = "";
		public ShopWindow window;

		public string TipText { get => _tiptext; set { _tiptext = value; OnPropertyChanged(); } }

		public string[] ShopItems => GetShopItems();

		public List<ShopItem> VisualItems => _visualitems;

		public string[] GetShopItems()
		{
			string[] shop_items = { "Choose an Item", "Cola : €1.00", "Fanta : €1.00", "Sprite : €1.00" };
			return (shop_items);
		}

		public void AddItem()
		{
			if (window == null)
				return;
			window.Close();
		}

	}
}
