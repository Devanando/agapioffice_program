﻿using AgapiOffice.Views.Journal;
using Avalonia.Controls;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgapiOffice.ViewModels.Journal
{
	class InsertJournalViewModel : ViewModelBase
	{

		public InsertJournal window;


		public void CloseWindow()
		{
			if (window == null)
				return;
			window.Close();
		}

	}
}
