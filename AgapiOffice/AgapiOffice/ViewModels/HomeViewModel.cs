﻿using AgapiOffice.Misc;
using AgapiOffice.Views;
using AgapiOffice.Views.Bring;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgapiOffice.ViewModels
{
	class HomeViewModel : ViewModelBase
	{
		private string _username = "";
		private readonly HomeBoolManager boolManager = new HomeBoolManager();

		public string UserName{ get => _username; set { _username = value; OnPropertyChanged(); } }

		public bool ShowClients { get { return boolManager.GetValue("showclients"); } set { boolManager.SetBool("showclients", value); OnPropertyChanged(); } }

		public static HomeView homeView;

		public void GoHome()
		{
			homeView.GoHome();
		}

		public void ShowClient()
		{
			homeView.ShowClient();
		}

		public void LogOut()
		{
			homeView.LogOut();
		}

		public void BringItem()
		{
			if (!homeView.IsVisible)
				return;
			homeView.BringItem();
		}

		public void SellItem()
		{
			if (!homeView.IsVisible)
				return;
			homeView.SellItem();
		}

		public void InsertJournal()
		{
			if (!homeView.IsVisible)
				return;
			homeView.InsertJournal();
		}

	}
}
