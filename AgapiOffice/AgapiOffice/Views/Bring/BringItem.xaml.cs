﻿using AgapiOffice.ViewModels.Bring;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using System.Collections.Generic;

namespace AgapiOffice.Views.Bring
{
	public class BringItem : UserControl
	{
		private string text;
		
		public string Text 
		{ 
			get => text;
			set 
			{ 
				text = value;
				var context = this.DataContext as BringItemViewModel;
				context.Item = value;
			} 
		}
		public List<BringItem> Items;
		public BringItemWindow Window;

		public BringItem()
		{
			this.InitializeComponent();
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
			this.DataContext = new BringItemViewModel() { BringItem = this };
		}

		public void DeleteSelf()
		{
			Items.Remove(this);
			Window.UpdateList();
		}

	}
}
