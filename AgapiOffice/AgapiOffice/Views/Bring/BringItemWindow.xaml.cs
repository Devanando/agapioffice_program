﻿using AgapiOffice.Misc;
using AgapiOffice.ViewModels.Bring;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.Platform;
using System;

namespace AgapiOffice.Views.Bring
{
	public class BringItemWindow : Window
	{
		public BringItemWindow()
		{
			this.InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
			this.DataContext = new BringItemWindowViewModel() { window = this };
			this.Find<ComboBox>("items").SelectionChanged += ItemsChanged;;
		}

		private void ItemsChanged(object sender, SelectionChangedEventArgs e)
		{
			string content = (string)e.AddedItems[0];
			if (content == "Choose an Item")
				return;
			var context = this.DataContext as BringItemWindowViewModel;
			context.VisualItems.Add(new BringItem() { Items = context.VisualItems, Text = content, Window = this});
			StackPanel panel = this.FindControl<StackPanel>("List");
			panel.Children.Clear();
			foreach (BringItem item in context.VisualItems)
				panel.Children.Add(item);
			this.Renderer.AddDirty(panel);
			var selector = sender as ComboBox;
			selector.SelectedIndex = 0;
		}

		public void UpdateList()
		{
			StackPanel panel = this.FindControl<StackPanel>("List");
			panel.Children.Clear();
			var context = this.DataContext as BringItemWindowViewModel;
			foreach (BringItem item in context.VisualItems)
				panel.Children.Add(item);
			this.Renderer.AddDirty(panel);
		}

		public void ExitWindow()
		{
			this.Close();
		}

	}
}
