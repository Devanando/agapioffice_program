﻿using AgapiOffice.ViewModels.Journal;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace AgapiOffice.Views.Journal
{
	public class InsertJournal : Window
	{
		public InsertJournal()
		{
			this.InitializeComponent();
#if DEBUG
			this.AttachDevTools();
#endif
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
			this.DataContext = new InsertJournalViewModel { window = this };
		}
	}
}
