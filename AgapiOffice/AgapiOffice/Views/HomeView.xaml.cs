﻿using AgapiOffice.Misc;
using AgapiOffice.Services;
using AgapiOffice.ViewModels;
using AgapiOffice.Views.Shop;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Markup.Xaml;

namespace AgapiOffice.Views
{
	public class HomeView : AgapiControl
	{
		// Init The Hot Key Manager
		private readonly KeyManager keyManager = new KeyManager();

		public HomeView()
		{
			this.InitializeComponent();
			this.DataContext = new HomeViewModel();
			HomeViewModel.homeView = this;
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);

			// Adding Key Manager Functions To Key Events
			KeyDown += keyManager.EventAddKey;
			KeyUp += keyManager.EventRemoveKey;

			// Adding Journal Function To ctrl + j
			Key[] jour0 = { Key.LeftCtrl, Key.J };
			keyManager.AddToHotKeys(jour0, JournalEntryPopUp);
			Key[] jour1 = { Key.RightCtrl, Key.J };
			keyManager.AddToHotKeys(jour1, JournalEntryPopUp);

			Visiblity += HomeView_Visiblity;
		}

		private void GetUsername()
		{
			((HomeViewModel)this.DataContext).UserName = AgapiConnect.UsernameQuerry();
		}

		private void HomeView_Visiblity(object sender, VisiblityEventArgs args)
		{
			if (args.IsVisible)
			{
				Focus();
				GetUsername();
			}
		}

		public void LogOut()
		{
			var context = this.Parent.DataContext as MainWindowViewModel;
			if (this.Parent.DataContext is MainWindowViewModel)
			{
				context.ShowHome = false;
				context.LoggedIn = false;
			}
		}

		public void GoHome()
		{
			((HomeViewModel)this.DataContext).ShowClients = false;
		}

		public int ShowClient()
		{
			((HomeViewModel)this.DataContext).ShowClients = true;
			return 0;
		}

		private int JournalEntryPopUp()
		{
			Out.Debug("Entered!");
			return 0;
		}

		public int SellItem()
		{
			var window = new ShopWindow
			{
				Width = 600,
				Height = 600
			};
			var parent = this.Parent;
			while (!(parent is Window))
				parent = parent.Parent;
			window.ShowDialog(parent as Window);
			return 0;
		}

		public int BringItem()
		{
			var window = new Bring.BringItemWindow
			{
				Width = 600,
				Height = 500
			};
			var parent = this.Parent;
			while (!(parent is Window))
				parent = parent.Parent;
			window.ShowDialog(parent as Window);
			return 0;
		}

		public int InsertJournal()
		{
			var window = new Journal.InsertJournal
			{
				Width = 500,
				Height = 400
			};
			var parent = this.Parent;
			while (!(parent is Window))
				parent = parent.Parent;
			window.ShowDialog(parent as Window);
			return 0;
		}

	}
}
