﻿using Avalonia;
using Avalonia.Controls;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgapiOffice.Views
{
	public class AgapiControl : Panel
	{

		public delegate void VisiblityEventHandler(object sender, VisiblityEventArgs args);

		public event VisiblityEventHandler Visiblity;

		public AgapiControl()
		{
			PropertyChanged += VisbilityEvent;
		}

		private void VisbilityEvent(object sender, AvaloniaPropertyChangedEventArgs e)
		{
			if (e.Property.Name == "IsVisible")
				Visiblity(sender, new VisiblityEventArgs() { IsVisible = (bool)e.NewValue});
		}

		public class VisiblityEventArgs : EventArgs
		{
			public bool IsVisible;
		}

	}
}
