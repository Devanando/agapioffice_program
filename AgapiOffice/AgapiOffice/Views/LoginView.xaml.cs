﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using ReactiveUI;
using System.Reactive;
using System;
using AgapiOffice.ViewModels;

namespace AgapiOffice.Views
{
	public class LoginView : AgapiControl
	{

		public LoginView()
		{
			this.InitializeComponent();
			this.DataContext = new LoginViewModel();
			LoginViewModel.loginView = this;
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
			Visiblity += LoginView_Visiblity;
		}

		private void LoginView_Visiblity(object sender, VisiblityEventArgs args)
		{
			if (args.IsVisible)
				Focus();
		}

		public void SetLogged()
		{
			var main = this.Parent.DataContext as MainWindowViewModel;
			if (this.Parent.DataContext is MainWindowViewModel)
			{
				main.LoggedIn = true;
				main.ShowHome = true;
			}
			var lmodel = this.DataContext as LoginViewModel;
			lmodel.LoginName = "";
			lmodel.LoginPassword = "";
		}

	}
}
