﻿using AgapiOffice.ViewModels.Shop;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using System.Collections.Generic;

namespace AgapiOffice.Views.Shop
{
	public class ShopItem : UserControl
	{
		private string text;
		public float Cost;
		public int Amount;

		public string Text
		{
			get => text;
			set
			{
				text = value;
				var context = this.DataContext as ShopItemViewModel;
				context.Item = value;
			}
		}
		public List<ShopItem> Items;
		public ShopWindow Window;

		public ShopItem()
		{
			this.InitializeComponent();
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
			this.DataContext = new ShopItemViewModel() { ShopItem = this };
		}

		public void DeleteSelf()
		{
			Items.Remove(this);
			Window.UpdateList();
		}

	}
}
