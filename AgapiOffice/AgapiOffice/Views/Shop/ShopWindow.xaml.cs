﻿using AgapiOffice.Misc;
using AgapiOffice.ViewModels.Shop;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using System.Collections.Generic;

namespace AgapiOffice.Views.Shop
{
	public class ShopWindow : Window
	{

		public ShopWindow()
		{
			this.InitializeComponent();
#if DEBUG
			this.AttachDevTools();
#endif
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
			this.DataContext = new ShopWindowViewModel() { window = this};
			this.FindControl<NumericUpDown>("Amounts").Text = "1";
			this.FindControl<Button>("Add").Click += AddClick;
			this.FindControl<TextBox>("Tip").PropertyChanged += TipChanged;
		}

		private void TipChanged(object sender, AvaloniaPropertyChangedEventArgs e)
		{
			AddCosts();
		}

		private void AddClick(object sender, Avalonia.Interactivity.RoutedEventArgs e)
		{
			ComboBox box = this.FindControl<ComboBox>("SelectItem");
			if (box.SelectedIndex == 0)
				return ;
			string subname = box.SelectedItem.ToString();
			string amount = this.FindControl<NumericUpDown>("Amounts").Text;
			string info =  subname + " x " + amount;
			if (this.FindControl<CheckBox>("OwnUse").IsChecked == true)
				info += " : Own Use";
			var context = this.DataContext as ShopWindowViewModel;
			context.VisualItems.Add(new ShopItem() {
				Items = context.VisualItems,
				Text = info,
				Window = this,
				Amount = int.Parse(amount),
				Cost = float.Parse(subname.Split('€')[1])
			});
			StackPanel panel = this.FindControl<StackPanel>("List");
			panel.Children.Clear();
			foreach (ShopItem item in context.VisualItems)
				panel.Children.Add(item);
			this.Renderer.AddDirty(panel);
			this.FindControl<ComboBox>("SelectItem").SelectedIndex = 0;
			this.FindControl<NumericUpDown>("Amounts").Text = "1";
			this.FindControl<CheckBox>("OwnUse").IsChecked = false;
			AddCosts();
		}

		private void AddCosts()
		{
			float costs = 0;
			try
			{
				costs += float.Parse(this.FindControl<TextBox>("Tip").Text);
			} catch
			{
				costs += 0;
			}
			var context = this.DataContext as ShopWindowViewModel;
			foreach (ShopItem si in context.VisualItems)
			{
				costs += si.Cost * si.Amount;
			}
			this.FindControl<TextBlock>("TotalCosts").Text = "Total Cost : €" + costs;
		}

		public void UpdateList()
		{
			StackPanel panel = this.FindControl<StackPanel>("List");
			panel.Children.Clear();
			var context = this.DataContext as ShopWindowViewModel;
			foreach (ShopItem item in context.VisualItems)
				panel.Children.Add(item);
			AddCosts();
			this.Renderer.AddDirty(panel);
		}

		public void ExitWindow()
		{
			this.Close();
		}

	}
}
