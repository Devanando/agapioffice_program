﻿using AgapiOffice.Misc;
using AgapiOffice.Services;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using AgapiOffice.ViewModels;
using Avalonia.Input;

namespace AgapiOffice.Views
{
    public class ClientInfo : AgapiControl
    {
        private readonly KeyManager keyManager = new KeyManager();
        public ClientInfo()
        {
            this.InitializeComponent();
            this.DataContext = new ClientInfoViewModel();
            ClientInfoViewModel.clientInfo = this;
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);

            // Adding Key Manager Functions To Key Events
            KeyDown += keyManager.EventAddKey;
            KeyUp += keyManager.EventRemoveKey;

            // Adding Journal Function To ctrl + j
            Key[] back0 = { Key.LeftCtrl, Key.B };
            keyManager.AddToHotKeys(back0, GoHome);
            Key[] back1 = { Key.RightCtrl, Key.B };
            keyManager.AddToHotKeys(back1, GoHome);

            Visiblity += ClientInfo_Visiblity;
        }

        private void ClientInfo_Visiblity(object sender, VisiblityEventArgs args)
        {
            if (args.IsVisible)
            {
                Focus();
            }
        }

        public int GoHome()
        {
            var context = this.Parent.DataContext as HomeViewModel;
            if (this.Parent.DataContext is HomeViewModel)
                context.ShowClients = false;
            return 0;
        }
    }
}
