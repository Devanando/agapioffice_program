﻿using AgapiOffice.Misc;
using AgapiOffice.Services;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using AgapiOffice.ViewModels;
using Avalonia.Input;

namespace AgapiOffice.Views
{
    public class MainView : AgapiControl
    {
        private readonly KeyManager keyManager = new KeyManager();
        public MainView()
        {
            this.InitializeComponent();
            this.DataContext = new MainViewModel();
            MainViewModel.mainView = this;
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);

            // Adding Key Manager Functions To Key Events
            KeyDown += keyManager.EventAddKey;
            KeyUp += keyManager.EventRemoveKey;


            Visiblity += MainView_Visiblity;
        }

        private void MainView_Visiblity(object sender, VisiblityEventArgs args)
        {
            if (args.IsVisible)
            {
                Focus();
            }
        }

        public void DayToggle()
        {
            ((MainViewModel)this.DataContext).Daytoggle = true;
            ((MainViewModel)this.DataContext).Nighttoggle = false;
        }

        public void NightToggle()
        {
            ((MainViewModel)this.DataContext).Nighttoggle = true;
            ((MainViewModel)this.DataContext).Daytoggle = false;
        }
    }
}
