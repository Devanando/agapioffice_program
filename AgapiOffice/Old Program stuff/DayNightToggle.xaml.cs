﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace AgapiWPF
{
    public class DayNightTransitionStartEventArgs : EventArgs
    {
        public DayNightTransitionStartEventArgs(DayNightToggle.DayNightValues transitionTo, TimeSpan timeSpan)
        {
            TransitionTo = transitionTo;
            AnimationDuration = timeSpan;
        }

        public TimeSpan AnimationDuration;
        public DayNightToggle.DayNightValues TransitionTo;
    }

    public class DayNightChangedEventArgs : EventArgs
    {
        public DayNightChangedEventArgs(DayNightToggle.DayNightValues newValue)
        {
            NewValue = newValue;
        }

        public DayNightToggle.DayNightValues NewValue;
    }

    /// <summary>
    /// Interaction logic for DayNightToggle.xaml
    /// </summary>
    public partial class DayNightToggle : UserControl
    {
        public delegate void DayNightTransitionStartHandler(object sender, DayNightTransitionStartEventArgs e);
        public event DayNightTransitionStartHandler OnDayNightTransitionStart;

        private void DayNightTransitionStart(DayNightValues toValue, TimeSpan timeSpan)
        {
            if (OnDayNightTransitionStart == null) return;
            DayNightTransitionStartEventArgs eventargs = new DayNightTransitionStartEventArgs(toValue, timeSpan);
            OnDayNightTransitionStart(this, eventargs);
        }

        public delegate void DayNightChangedHandler(object sender, DayNightChangedEventArgs e);
        public event DayNightChangedHandler OnDayNightChanged;

        private void DayNightChanged(DayNightValues newValue)
        {
            if (OnDayNightChanged == null) return;
            DayNightChangedEventArgs eventargs = new DayNightChangedEventArgs(newValue);
            OnDayNightChanged(this, eventargs);
        }

        double AnimationDuration = 500; // transition duration in ms

        double ImageWidth = 550;
        double ImageHeight = 140;
        double SwitchDiameter = 100;
        double SwitchDayTop = 20;
        double SwitchDayLeft = 22;
        double SwitchNightTop = 20;
        double SwitchNightLeft = 550 - 22 - 100;

        double ActualImageWidth = 550;
        double ActualImageHeight = 140;
        double ActualSwitchDiameter = 100;
        double ActualSwitchDayTop = 20;
        double ActualSwitchDayLeft = 22;
        double ActualSwitchNightTop = 20;
        double ActualSwitchNightLeft = 550 - 22 - 100;

        double ZoomFactor = 1;

        public enum DayNightValues
        {
            Day, Night
        }

        public static readonly DependencyProperty DayNightProperty = DependencyProperty.Register("DayNight", typeof(DayNightValues), typeof(DayNightToggle), new UIPropertyMetadata(DayNightValues.Day));

        public DayNightValues DayNight
        {
            get
            {
                return (DayNightValues) GetValue(DayNightProperty);
            }
            set
            {
                SetValue(DayNightProperty, value);
            }
        }

        void InitializeUserControl()
        {
            this.SizeChanged += DayNightToggle_SizeChanged;

            if (!double.IsNaN(this.Width))
            {
                ZoomFactor = this.Width / this.ImageWidth;
            }
            else if (!double.IsNaN(this.Height))
            {
                ZoomFactor = this.Height / this.ImageHeight;
            }

            if (ZoomFactor != 1)
            {
                ActualImageWidth = ImageWidth * ZoomFactor;
                ActualImageHeight = ImageHeight * ZoomFactor;
                ActualSwitchDiameter = SwitchDiameter * ZoomFactor;
                ActualSwitchDayTop = SwitchDayTop * ZoomFactor;
                ActualSwitchDayLeft = SwitchDayLeft * ZoomFactor;
                ActualSwitchNightTop = SwitchNightTop * ZoomFactor;
                ActualSwitchNightLeft = SwitchNightLeft * ZoomFactor;
            }
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                var day_uri = new Uri("pack://application:,,,/Images/switch_day.png");
                var day_bitmap = new BitmapImage(day_uri);
                RenderOptions.SetBitmapScalingMode(ImageDay, BitmapScalingMode.Fant);
                ImageDay.Source = day_bitmap;

                var night_uri = new Uri("pack://application:,,,/Images/switch_night.png");
                var night_bitmap = new BitmapImage(night_uri);
                RenderOptions.SetBitmapScalingMode(ImageNight, BitmapScalingMode.Fant);
                ImageNight.Source = night_bitmap;
            }
            //UpdateEllipse();
        }

        void UpdateEllipse()
        {
            EllipseButtonWhite.Width = ActualSwitchDiameter;
            EllipseButtonWhite.Height = ActualSwitchDiameter;
            EllipseButtonWhite.HorizontalAlignment = HorizontalAlignment.Left;
            EllipseButtonWhite.VerticalAlignment = VerticalAlignment.Top;
            EllipseButtonBlack.Width = ActualSwitchDiameter;
            EllipseButtonBlack.Height = ActualSwitchDiameter;
            EllipseButtonBlack.HorizontalAlignment = HorizontalAlignment.Left;
            EllipseButtonBlack.VerticalAlignment = VerticalAlignment.Top;
            if (DayNight == DayNightValues.Day) // set ellipse positions to day position and night opacity to 0
            {
                //ellipse.Margin = new Thickness(ActualSwitchDayLeft, ActualSwitchDayTop, 0, 0);
                Canvas.SetLeft(GridEllipse, ActualSwitchDayLeft);
                Canvas.SetTop(GridEllipse, ActualSwitchDayTop);
                ImageNight.Opacity = 0;
            }
            else // set ellipse positions to night position and night opacity to 1
            {
                //ellipse.Margin = new Thickness(ActualSwitchNightLeft, ActualSwitchNightTop, 0, 0);
                Canvas.SetLeft(GridEllipse, ActualSwitchNightLeft);
                Canvas.SetTop(GridEllipse, ActualSwitchNightTop);
                ImageNight.Opacity = 1;
            }
        }

        void DoAnimations()
        {
            Storyboard sbLeft = new Storyboard();
            Storyboard sbSwitchOpacity = new Storyboard();
            Storyboard sbImageOpacity = new Storyboard();
            DoubleAnimation switchAnimation;
            DoubleAnimation switchOpacity;
            DoubleAnimation imageOpacity;
            if (DayNight == DayNightValues.Day)
            {
                switchAnimation = new DoubleAnimation();
                switchAnimation.From = ActualSwitchDayLeft;
                switchAnimation.To = ActualSwitchNightLeft;
                switchAnimation.Duration = new Duration(TimeSpan.FromMilliseconds(AnimationDuration));
                switchAnimation.AutoReverse = false;
                switchAnimation.RepeatBehavior = new RepeatBehavior((double)1);
                // https://docs.microsoft.com/en-us/dotnet/framework/wpf/graphics-multimedia/easing-functions
                CircleEase ce = new CircleEase();
                CubicEase cue = new CubicEase();
                SineEase sne = new SineEase();
                QuadraticEase qde = new QuadraticEase();
                QuarticEase qte = new QuarticEase();
                qte.EasingMode = EasingMode.EaseInOut;
                switchAnimation.EasingFunction = qte;
                Storyboard.SetTargetProperty(switchAnimation, new PropertyPath("(Canvas.Left)"));
                sbLeft.Children.Add(switchAnimation);
                GridEllipse.BeginStoryboard(sbLeft);

                /*DoubleAnimation switchAnimation2 = new DoubleAnimation();
                switchAnimation2.From = ActualSwitchNightLeft;
                switchAnimation2.To = ActualSwitchDayLeft;
                switchAnimation2.Duration = new Duration(TimeSpan.FromMilliseconds(AnimationDuration));
                switchAnimation2.AutoReverse = false;
                switchAnimation2.RepeatBehavior = new RepeatBehavior((double)1);
                // https://docs.microsoft.com/en-us/dotnet/framework/wpf/graphics-multimedia/easing-functions
                BounceEase bne = new BounceEase();
                //bne.EasingMode = EasingMode.EaseInOut;
                switchAnimation2.EasingFunction = bne;
                Storyboard.SetTargetProperty(switchAnimation2, new PropertyPath("(Canvas.Top)"));
                sbLeft.Children.Add(switchAnimation2);*/

                imageOpacity = new DoubleAnimation();
                imageOpacity.From = 0;
                imageOpacity.To = 1.0;
                imageOpacity.Duration = new Duration(TimeSpan.FromMilliseconds(AnimationDuration));
                imageOpacity.AutoReverse = false;
                imageOpacity.RepeatBehavior = new RepeatBehavior((double)1);
                CircleEase imgease = new CircleEase();
                imgease.EasingMode = EasingMode.EaseIn;
                imageOpacity.EasingFunction = imgease;
                Storyboard.SetTargetProperty(imageOpacity, new PropertyPath("Opacity"));
                sbImageOpacity.Children.Add(imageOpacity);
                ImageNight.BeginStoryboard(sbImageOpacity);

                switchOpacity = new DoubleAnimation();
                switchOpacity.From = 0;
                switchOpacity.To = 1;
                switchOpacity.Duration = new Duration(TimeSpan.FromMilliseconds(AnimationDuration));
                switchOpacity.AutoReverse = false;
                switchOpacity.RepeatBehavior = new RepeatBehavior((double)1);
                Storyboard.SetTargetProperty(switchOpacity, new PropertyPath("Opacity"));
                sbSwitchOpacity.Children.Add(switchOpacity);
                
                sbSwitchOpacity.Completed += SbSwitchOpacity_Completed_Night;
                EllipseButtonWhite.BeginStoryboard(sbSwitchOpacity);

                // fire transition start event
                DayNightTransitionStart(DayNightValues.Night, TimeSpan.FromMilliseconds(AnimationDuration));
            }
            else
            {
                switchAnimation = new DoubleAnimation();
                switchAnimation.From = ActualSwitchNightLeft;
                switchAnimation.To = ActualSwitchDayLeft;
                switchAnimation.Duration = new Duration(TimeSpan.FromMilliseconds(AnimationDuration));
                switchAnimation.AutoReverse = false;
                switchAnimation.RepeatBehavior = new RepeatBehavior((double)1);
                QuarticEase qte = new QuarticEase();
                qte.EasingMode = EasingMode.EaseInOut;
                switchAnimation.EasingFunction = qte;
                Storyboard.SetTargetProperty(switchAnimation, new PropertyPath("(Canvas.Left)"));
                sbLeft.Children.Add(switchAnimation);
                GridEllipse.BeginStoryboard(sbLeft);

                imageOpacity = new DoubleAnimation();
                imageOpacity.From = 1;
                imageOpacity.To = 0;
                imageOpacity.Duration = new Duration(TimeSpan.FromMilliseconds(AnimationDuration));
                imageOpacity.AutoReverse = false;
                imageOpacity.RepeatBehavior = new RepeatBehavior((double)1);
                CircleEase imgease = new CircleEase();
                imgease.EasingMode = EasingMode.EaseIn;
                imageOpacity.EasingFunction = imgease;
                Storyboard.SetTargetProperty(imageOpacity, new PropertyPath("Opacity"));
                sbImageOpacity.Children.Add(imageOpacity);
                ImageNight.BeginStoryboard(sbImageOpacity);

                switchOpacity = new DoubleAnimation();
                switchOpacity.From = 1;
                switchOpacity.To = 0;
                switchOpacity.Duration = new Duration(TimeSpan.FromMilliseconds(AnimationDuration));
                switchOpacity.AutoReverse = false;
                switchOpacity.RepeatBehavior = new RepeatBehavior((double)1);
                Storyboard.SetTargetProperty(switchOpacity, new PropertyPath("Opacity"));
                sbSwitchOpacity.Children.Add(switchOpacity);
                sbSwitchOpacity.Completed += SbSwitchOpacity_Completed_Day; // fire changed event in this handler
                EllipseButtonWhite.BeginStoryboard(sbSwitchOpacity);


                // transition start event
                DayNightTransitionStart(DayNightValues.Day, TimeSpan.FromMilliseconds(AnimationDuration));
                
            }
        }

        private void SbSwitchOpacity_Completed_Night(object sender, EventArgs e)
        {
            DayNight = DayNightValues.Night;
            DayNightChanged(DayNightValues.Night);
        }

        private void SbSwitchOpacity_Completed_Day(object sender, EventArgs e)
        {
            DayNight = DayNightValues.Day;
            DayNightChanged(DayNightValues.Day);
        }

        private void DayNightToggle_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ImageDay.Width = e.NewSize.Width;
            ImageDay.Height = e.NewSize.Height;
            ImageNight.Width = e.NewSize.Width;
            ImageNight.Height = e.NewSize.Height;
            ZoomFactor = e.NewSize.Width / this.ImageWidth;
            ActualImageWidth = ImageWidth * ZoomFactor;
            ActualImageHeight = ImageHeight * ZoomFactor;
            ActualSwitchDiameter = SwitchDiameter * ZoomFactor;
            ActualSwitchDayTop = SwitchDayTop * ZoomFactor;
            ActualSwitchDayLeft = SwitchDayLeft * ZoomFactor;
            ActualSwitchNightTop = SwitchNightTop * ZoomFactor;
            ActualSwitchNightLeft = SwitchNightLeft * ZoomFactor;
            MainCanvas.Width = ActualImageWidth;
            MainCanvas.Height = ActualImageHeight;
            ImageDay.Width = ActualImageWidth;
            ImageDay.Height = ActualImageHeight;
            ImageNight.Width = ActualImageWidth;
            ImageNight.Height = ActualImageHeight;
            UpdateEllipse();
        }


        public DayNightToggle()
        {
            InitializeComponent();
            InitializeUserControl();
        }

        private void UserControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DoAnimations();
        }
    }
}
