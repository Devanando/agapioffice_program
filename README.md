This program will be the booking program that will be used at Agapi B.V. 

It will consist off windows GUI program made with .NET Core, avalonia and Microsoft SQL or MYSQL. The database will be run from another location on a Raspberry pi 4 and will only be able to change/create/remove tables locally since root acces is only avaiable locally on the raspberry pi. 

To do:

[X] Figure out .Net Core.

[X] Figure out Avalonia for GUI.

[X] Setup raspberry pi with a database.

[V] Mini-shop in the program.

[] Intake and client information.

[] Booking part showing both shifts (day and night) for all the week with status.

[] Administration page for invoices